class DistroInfo extends HTMLElement {
  set info({page, info}) {
    this._page = page;
    this._info = info;
    this.render();
  }

  render() {
    const info = this._info.map(({title, value}) => `<tr><td><b>${title}</b></td><td>${value}</td></tr>`)
    this.innerHTML = `<div class="card">
      <div class="card-content">
        <div style="padding: 1rem 4rem">
          <img class="responsive-img" src="/img/logo/${this._page}.png">
        </div>
        <table class="striped" style="margin-top: 2rem">
          <tbody>${info.join("\n")}</tbody>
        </table>
      </div>
    </div>`
  }
}

customElements.define('distro-info', DistroInfo);