class MyContent extends HTMLElement {
  succesResult({name, detail, page, info}) {
    this.innerHTML = `
    <div class="row">
      <div class="col s12">
        <img class="responsive-img" src="img/screenshot/${page}.png" alt="${name} Screenshot" style="width: 100%">
      </div>
    </div>
    <div class="row">
      <div class="col s12 l8" id="distro-summary"></div>
      <div class="col s12 l4" id="distro-info"></div>
    </div>`

    // Create summary
    const createDistroSummary = document.createElement("distro-summary");
    createDistroSummary.summary = {name, detail}
    $('#distro-summary').append(createDistroSummary);

    // Craete info
    const createDistroInfo = document.createElement("distro-info");
    createDistroInfo.info = {page, info}
    $('#distro-info').append(createDistroInfo);
  }

  errorResult(message) {
    this.innerHTML = `
    <div class="center-align grey-text text-darken-2">
      <i class="large material-icons center-align">warning</i>
      <h3>${message}</h3>
    </div>
    `
  }
}

customElements.define('my-content', MyContent);