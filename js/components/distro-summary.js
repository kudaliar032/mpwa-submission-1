class DistroSummary extends HTMLElement {
  set summary(summary) {
    this._summary = summary;
    this.render();
  }

  render() {
    let detail = this._summary.detail.map(val => `<p>${val}</p>`)
    this.innerHTML = `<h3>${this._summary.name}</h3>
      <section style="text-align: justify">
        ${detail.join("\n")}
      </section>`;
  }
}

customElements.define('distro-summary', DistroSummary);