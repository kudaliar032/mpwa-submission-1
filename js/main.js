$(document).ready(() => {
  // Load page content
  var page = window.location.hash.substr(1);
  if (page === "") page = "ubuntu";
  loadPage(page);

  // Activate sidebar nav
  const elems = $('.sidenav');
  M.Sidenav.init(elems);
  loadNav();
})

const loadNav = () => {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState === 4) {
      if (this.status !== 200) return;

      document.querySelectorAll(".topnav, .sidenav").forEach(elm => {
        elm.innerHTML = xhttp.responseText;
      });

      // Daftarkan event listener untuk setiap tautan menu
      document.querySelectorAll(".sidenav a, .topnav a").forEach(elm => {
        elm.addEventListener("click", event => {
          // Tutup sidenav
          const sidenav = $(".sidenav");
          M.Sidenav.getInstance(sidenav).close();

          // Muat konten halaman yang dipanggil
          const page = event.target.getAttribute("href").substr(1);
          loadPage(page);
        })
      })
    }
  };
  xhttp.open("GET", "nav.html", true);
  xhttp.send();
}

const loadPage = page => {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState === 4) {
      const content = $("#body-content");
      if (this.status === 200) {
        const data = JSON.parse(xhttp.responseText)
        $('#distro-screenshot').attr("src", `img/screenshot/${page}.png`)
        document.querySelector("my-content").succesResult({...data, page});
      } else if (this.status === 404) {
        document.querySelector("my-content").errorResult("Halaman tidak ditemukan.");
      } else {
        document.querySelector("my-content").errorResult("Ups.. halaman tidak dapat diakses.");
      }
    }
  };
  xhttp.open("GET", "data/" + page + ".json", true);
  xhttp.send();
}