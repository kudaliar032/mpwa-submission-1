const CACHE_NAME = "linux-distro";
const urlsToCache = [
  "/",
  "/index.html",
  "/nav.html",
  "/manifest.json",
  "/css/custom.css",
  "/css/materialize.min.css",
  "/data/fedora.json",
  "/data/opensuse.json",
  "/data/rhel.json",
  "/data/ubuntu.json",
  "/img/logo/fedora.png",
  "/img/logo/opensuse.png",
  "/img/logo/rhel.png",
  "/img/logo/ubuntu.png",
  "/img/screenshot/fedora.png",
  "/img/screenshot/opensuse.png",
  "/img/screenshot/rhel.png",
  "/img/screenshot/ubuntu.png",
  "/img/icons/icon-72x72.png",
  "/img/icons/icon-96x96.png",
  "/img/icons/icon-128x128.png",
  "/img/icons/icon-144x144.png",
  "/img/icons/icon-152x152.png",
  "/img/icons/icon-192x192.png",
  "/img/icons/icon-384x384.png",
  "/img/icons/icon-512x512.png",
  "/js/components/distro-info.js",
  "/js/components/distro-summary.js",
  "/js/components/my-content.js",
  "/js/jquery-3.5.1.min.js",
  "/js/main.js",
  "/js/materialize.min.js",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://fonts.gstatic.com/s/materialicons/v50/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2"
]

self.addEventListener("install", function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
      return cache.addAll(urlsToCache)
    })
  )
})

self.addEventListener("fetch", function (event) {
  event.respondWith(
    caches.match(event.request, {cacheName: CACHE_NAME})
      .then(function (response) {
        if (response) {
          console.log("ServiceWorker: Gunakan aset dari cache: ", response.url)
          return response
        }

        console.log("ServiceWorker: Memuat aset dari server: ", event.request.url)
        return fetch(event.request)
      })
  )
})

self.addEventListener("activate", function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheName) {
      return Promise.all(
        cacheName.map(function (cacheName) {
          if (cacheName !== CACHE_NAME) {
            console.log("ServiceWorker: cache "+cacheName+" dihapus")
            return caches.delete(cacheName);
          }
        })
      )
    })
  )
})